FROM ricardocg94/tf-base:latest
ENV CLOUD_SDK_VERSION 303.0.0


COPY gpc-key.json .

# Downloading gcloud package
RUN curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz > /tmp/google-cloud-sdk.tar.gz

# Installing the package
RUN mkdir -p /usr/local/gcloud \
  && tar -C /usr/local/gcloud -xvf /tmp/google-cloud-sdk.tar.gz \
  && /usr/local/gcloud/google-cloud-sdk/install.sh

# Adding the package path to local
ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin 

RUN cp gpc-key.json /root/.config/gcloud \
    && export GOOGLE_APPLICATION_CREDENTIALS='/root/.config/gcloud/gpc-key.json'

ENV GOOGLE_APPLICATION_CREDENTIALS='/root/.config/gcloud/gpc-key.json'

VOLUME ["/root/.config"]