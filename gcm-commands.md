# Commonly used GCP commands

- gcloud auth list  \\ get active account 
- gcloud config set account `ACCOUNT`
- gcloud init 
- gcloud projects list   \\ List the projects on the account configured 
- gsutil list 
- gcloud compute instances list
- gcloud auth activate-service-account test-service-account@google.com --key-file=/path/key.json --project=testproject \\ Activar cuenta desde consolda de comandos (no interactivo)
- gcloud iam service-accounts list
- gcloud auth application-default login 

# Inspec commands for GCP
- inspec init profile name
- inspec detect -t gcp://

